from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
import logging
logger = logging.getLogger("Celery")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sipsem.settings.development')

app = Celery('sipsem')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
