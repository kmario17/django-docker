const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");;
const BundleTracker = require('webpack-bundle-tracker');


module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname,'/dist'),
    filename:'[name]-[hash].js',
		publicPath:'http://localhost:8080/frontend/dist/'
	},
	
  module: {
    rules: [
      {
        test: /\.js|jsx$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: [
              '@babel/plugin-proposal-class-properties'
           ]
          }
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  plugins:[
		new HtmlWebpackPlugin({
			template: "index.html"
		}),
		new BundleTracker({
			filename: '../src/webpack-stats.json'
		})
	],

	devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
};