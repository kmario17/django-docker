import React from 'react';


class Form extends React.Component {
    state = {
         nombre:'',
         cc:''
    }

    handleSubmit = (event) => {
      event.preventDefault();
      console.log(JSON.stringify(this.state))
    }
    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
          });  
    }
    render(){
        return(
            <div>
             <h3>Formulario.</h3>
             { JSON.stringify(this.state) }
             <form onSubmit={this.handleSubmit}>
                <input 
                    name="nombre" 
                    type="text"  
                    placeholder="nombre"
                    value={this.state.nombre} 
                    onChange={this.handleChange}/>
                <br/>
                <input 
                    name="cc" 
                    type="number"
                    placeholder="c.c" 
                    value={this.state.cc}
                    onChange={this.handleChange}/>
                 <br/>   
                <button >Enviar</button>
             </form>
            </div>
        )
    }
}

export default Form;